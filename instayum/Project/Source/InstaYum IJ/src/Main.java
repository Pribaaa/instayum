import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.ServerAddress;

public class Main {
    public static void main(String[] args)
    {
        KonekcijaOracleETF db = new KonekcijaOracleETF();
        try {
            db.loadDriver(); //napuni klasu drajver
            db.connect(); //uspostavi konekciju - konekcija
            db.getStatement(db.konekcija);
            db.createKorisnik();
            db.createObjave();
            db.createSlikeHrane();
            db.createKomentari();
            db.createKorisnik_Logovi();
            db.createKorisnik_Komentari();
            db.createObjave_Logovi();
        } catch (Exception e) { e.printStackTrace(); }
        Servisi.createLog();
        DBCollection collection = KonekcijaMongoDB.db.getCollection("Log");

        System.out.println("BasicDBObject example...");
        BasicDBObject document = new BasicDBObject();
        document.put("Name", "Damir");
        collection.insert(document);

        BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("Name", "Damir");		// Pronalazak imena Damir
            DBCursor cursor = collection.find(searchQuery);
            while (cursor.hasNext())
            {   System.out.println(cursor.next());   }
    }
}
