import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.digester.Digester;

public class Servisi {

    public Statement getStatement() {
        try {
            // za datu konekciju kreira i vraća objekat klase Statement
            KonekcijaOracleETF.iskaz = KonekcijaOracleETF.konekcija.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return KonekcijaOracleETF.iskaz;
    }


    public void createDatabase() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE DATABASE InstaYum");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void createKorisnik() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE TABLE Korisnik(KorisnikID int, " +
                    "Username char(255) not null, DatumRodjenja date not null, " +
                    "Email char(255) not null, CONSTRAINT KorisnikID PRIMARY KEY (KorisnikID))");
            System.out.println("Kreirana tabela Korisnik");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createObjave() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE TABLE Objave(ObjaveID int," +
                    "OpisHrane char(255) not null, DatumObjave date not null," +
                    "Recept char(255) not null, Komentari char (255) not null," +
                    "Ocjena int not null,Objave_KorisnikaID int REFERENCES Korisnik (KorisnikID)," +
                    "CONSTRAINT ObjaveID PRIMARY KEY (ObjaveID))");
            System.out.println("Kreirana tabela Objave");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createSlikeHrane() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE TABLE SlikeHrane(SlikaID int," +
                    "OpisSlike char(255) not null, Slika blob not null," +
                    "DatumDodavanja date not null, ObjavaObjaveID int REFERENCES Objave (ObjaveID))");
            System.out.println("Kreirana tabela SlikeHrane");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createKomentari() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE TABLE Komentari(KomentarID int," +
                    "KorisnikID int not null, Komentar char(255) not null," +
                    "CONSTRAINT KomentarID PRIMARY KEY (KomentarID))");
            System.out.println("Kreirana tabela Komentari");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void createKorisnik_Logovi() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE TABLE Korisnik_Logovi(KorisnikKorisnikID int REFERENCES Korisnik (KorisnikID)," +
                    "LogoviLogID int not null)");
            System.out.println("Kreirana tabela Korisnik_logovi");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createKorisnik_Komentari() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE TABLE Korisnik_Komentari(KorisnikKorisnikID int REFERENCES Korisnik (KorisnikID)," +
                    "KomentariKomentarID int REFERENCES Komentari (KomentarID))");
            System.out.println("Kreirana tabela Korisnik_Komentari");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createObjave_Logovi() {
        try {
            KonekcijaOracleETF.iskaz.executeQuery("CREATE TABLE Objave_Logovi(ObjaveObjavaID int not null REFERENCES Objave (ObjaveID)," +
                    "LogoviLogID int not null)");
            System.out.println("Kreirana tabela Objave_Logovi");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static public void createLog() {
        try {
            KonekcijaMongoDB.db.createCollection("Log", new BasicDBObject());
            DBCollection table = KonekcijaMongoDB.db.getCollection("Log");
            System.out.println("Kreinrana kolekcija Log");
        } catch (Exception e) {
            System.out.println(e.getClass().getName() + ":" + e.getMessage());
        }
    }
}

