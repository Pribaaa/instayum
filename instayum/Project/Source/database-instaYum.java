public Statement getStatement() {
     try { 
         // za datu konekciju kreira i vraća objekat klase Statement 
         iskaz = konekcija.createStatement(); } 
         catch (SQLException e) { 
             e.printStackTrace(); 
             } 
             return iskaz; 
        }


/*public  void createDatabase() { 
   try { 
       iskaz.executeQuery("CREATE DATABASE InstaYum"); } catch (SQLException e) { 
            e.printStackTrace(); 
          } 
        }*/


public  void createKorisnik() {
     try {
          iskaz.executeQuery("CREATE TABLE Korisnik(KorisnikID int," + "Username char(255) not null,     
          DatumRodjenja date not null,
          Email char(255) not null),
          CONSTRAINT korisnik_pk PRIMARY KEY (KorisnikID)"); 
          } catch (SQLException e) {
               e.printStackTrace(); 
               } 
            }

public  void createObjave() {
     try {
          iskaz.executeQuery("CREATE TABLE Objave(ObjaveID int," + "OpisHrane char(255) not null,
          DatumObjave date not null,
          Recept char(255) not null,
          Komentari char (255) not null,
          Ocjena int not null,
          KorisnikKorisnikaID int REFERENCES Korisnik (KorisnikID)),
          CONSTRAINT objave_pk PRIMARY KEY (ObjaveID)"); 
          } catch (SQLException e) {
               e.printStackTrace(); 
               } 
            }

public  void createSlikeHrane() {
     try {
          iskaz.executeQuery("CREATE TABLE SlikeHrane(SlikaID int," + "OpisSlike char(255) not null,
          Slika blob not null,
          DatumDodavanja date not null,
          ObjavaObjaveID int REFERENCES Objave (ObjaveID)),
          CONSTRAINT slika_pk PRIMARY KEY (SlikaID)"); 
          } catch (SQLException e) {
               e.printStackTrace(); 
               } 
            }

public  void createKomentari() {
    try {
            iskaz.executeQuery("CREATE TABLE Komentari(KomentarID int," + "KorisnikID int not null,
            Komentar char(255) not null),
            CONSTRAINT komentar_pk PRIMARY KEY (KomentarID)"); 
            } catch (SQLException e) {
                e.printStackTrace(); 
                } 
            }

public  void createLogovi() {
    try {
            iskaz.executeQuery("CREATE TABLE Logovi(LogID int," + "Log char(255) not null), 
            CONSTRAINT log_pk PRIMARY KEY (LogID)"); 
            } catch (SQLException e) {
                e.printStackTrace(); 
                } 
            }

public  void createKorisnik_Logovi() {
    try {
            iskaz.executeQuery("CREATE TABLE Korisnik_Logovi(KorisnikKorisnikID int REFERENCES Korisnik (KorisnikID),
            LogoviLogID int REFERENCES Logovi (LogID))");
            } catch (SQLException e) {
                e.printStackTrace(); 
                } 
            }            

public  void createKorisnik_Komentari() {
    try {
            iskaz.executeQuery("CREATE TABLE Korisnik_Komentari(KorisnikKorisnikID int REFERENCES Korisnik (KorisnikID),
            KomentariKomentarID int REFERENCES Komentari (KomentarID))");
            } catch (SQLException e) {
                e.printStackTrace(); 
                } 
            } 

public  void createObjave_Logovi() {
    try {
            iskaz.executeQuery("CREATE TABLE Objave_Logovi(ObjaveObjavaID int REFERENCES Objave (ObjavaID),
            LogoviLogID int REFERENCES Logovi (LogID))");
            } catch (SQLException e) {
                e.printStackTrace(); 
                } 
            } 
