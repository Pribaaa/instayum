const express = require('express');
const router = express.Router();
const app = express();
const bodyparser = require("body-parser");
var baza
app.use('/', express.static('static'));

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

var Connection = require('tedious').Connection;
var Request = require('tedious').Request;

// Create connection to database
/*var config = 
   {
     userName: 'someuser', // update me
     password: 'somepassword', // update me
     server: 'edmacasqlserver.database.windows.net', // update me
     options: 
        {
           database: 'somedb' //update me
           , encrypt: true
        }
   }
var connection = new Connection(config);*/

app.get('/getStatement', function(request, response) {
    baza.collection('InstaYum').find().toArray((err, stat) => {
        if (err) return console.log(err);
        response.setHeader('Content-Type', 'application/json');
        response.send(stat);
    })
});

app.get('/getKorisnik', function(request, response) {
    baza.collection('InstaYum').find().toArray((err, korisnik) => {
        if (err) return console.log(err);
        response.setHeader('Content-Type', 'application/json');
        response.send(korisnik);
    })
});

app.get('/getObjave', function(request, response) {
    baza.collection('InstaYum').find().toArray((err, objava) => {
        if (err) return console.log(err);
        response.setHeader('Content-Type', 'application/json');
        response.send(objava);
    })
});

app.get('/getSlikeHrane', function(request, response) {
    baza.collection('InstaYum').find().toArray((err, slika) => {
        if (err) return console.log(err);
        response.setHeader('Content-Type', 'application/json');
        response.send(slika);
    })
});

app.get('/getKomentare', function(request, response) {
    baza.collection('InstaYum').find().toArray((err, komentar) => {
        if (err) return console.log(err);
        response.setHeader('Content-Type', 'application/json');
        response.send(komentar);
    })
});

app.post('/dodajKorisnika', function(req, res){
    req.body._id = null;
    var korisnik = req.body;
    baza.collection('InstaYum').insert(korisnik, function(err, data){
        if(err) return console.log(err);
        res.setHeader('Content-Type', 'application/json');
        res.send(korisnik);
    })
});

app.post('/dodajObjavu', function(req, res){
    req.body._id = null;
    var korisnik = req.body;
    baza.collection('InstaYum').insert(korisnik, function(err, data){
        if(err) return console.log(err);
        res.setHeader('Content-Type', 'application/json');
        res.send(korisnik);
    })
});

app.post('/dodajSlikuHrane', function(req, res){
    req.body._id = null;
    var korisnik = req.body;
    baza.collection('InstaYum').insert(korisnik, function(err, data){
        if(err) return console.log(err);
        res.setHeader('Content-Type', 'application/json');
        res.send(korisnik);
    })
});

app.post('/dodajKomentar/:id', function(req, res){
    req.body.id = req.params.id;
    var korisnik = req.body;
    baza.collection('InstaYum').insert(korisnik, function(err, data){
        if(err) return console.log(err);
        res.setHeader('Content-Type', 'application/json');
        res.send(korisnik);
    })
});

app.delete('/izbrisiKorisnika/:korisnik_id', function(req, res){
    baza.collection('InstaYum').remove({_id: new ID(req.params.korisnik_id)},
    function(err, data){
        res.json(data);
    });
});

app.delete('/izbrisiObjavu/:objava_id', function(req, res){
    baza.collection('InstaYum').remove({_id: new ID(req.params.objava_id)},
    function(err, data){
        res.json(data);
    });
});

app.delete('/izbrisiSlikuHrane/:slika_id', function(req, res){
    baza.collection('InstaYum').remove({_id: new ID(req.params.slika_id)},
    function(err, data){
        res.json(data);
    });
});

app.delete('/izbrisiKorisnikKomentar/:komentar_id', function(req, res){
    baza.collection('InstaYum').remove({_id: new ID(req.params.komentar_id)},
    function(err, data){
        res.json(data);
    });
});

app.put('/updateKorisnik/:korisnik_id', function(req, res){
    req.body.korisnik_id = req.params.korisnik_id;   
    baza.collection('InstaYum').findAndModify(
       {_id: new ID(req.params.korisnik_id)},
       [['_id','asc']],
       {$set : {ime:req.body.ime,prezime:req.body.prezime,username:req.body.username,email:req.body.email}},
       function(err, doc) {
           if (err){
               console.warn(err.message); 
           }else{
               res.json(doc);
           }
       });
});

app.put('/updateObjavu/:objava_id', function(req, res){
    req.body.objava_id = req.params.objava_id;   
    baza.collection('InstaYum').findAndModify(
       {_id: new ID(req.params.objava_id)},
       [['_id','asc']],
       {$set : {opis:req.body.opis,recept:req.body.recept}},
       function(err, doc) {
           if (err){
               console.warn(err.message); 
           }else{
               res.json(doc);
           }
       });
});

app.put('/updateSlikuHrane/:slika_id', function(req, res){
    req.body.slika_id = req.params.slika_id;   
    baza.collection('InstaYum').findAndModify(
       {_id: new ID(req.params.slika_id)},
       [['_id','asc']],
       {$set : {slika:req.body.slika,opis_slike:req.body.opis_slike}},
       function(err, doc) {
           if (err){
               console.warn(err.message); 
           }else{
               res.json(doc);
           }
       });
});

app.put('/updateKorisnik/:komentar_id/;korisnik_id', function(req, res){
    req.body.komentar_id = req.params.komentar_id;
    req.body.korisnik_id = req.params.korisnik_id;      
    baza.collection('InstaYum').findAndModify(
       {_id: new ID(req.params.komentar_id)},
       [['_id','asc']],
       {$set : {komentar:req.body.komentar}},
       function(err, doc) {
           if (err){
               console.warn(err.message); 
           }else{
               res.json(doc);
           }
       });
});