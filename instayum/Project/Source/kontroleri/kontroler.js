function DashboardController($scope, $rootScope, $http){
    

    refresh_korisnik();
    refresh_objave();

    function refresh_korisnik(){
        $http.get('/getKorisnik').then(function(res){
            $scope.korisnik_list = res.data;
        }),
        function(res){
            alert(res.status);
        }
    }

    function refresh_objave(){
        $http.get('/getObjave').then(function(res){
            $scope.objava_list = res.data;
        }),
        function(res){
            alert(res.status);
        }
    }

    function refresh_slike(){
        $http.get('/getSlikeHrane').then(function(res){
            $scope.slika_list = res.data;
        }),
        function(res){
            alert(res.status);
        }
    }
    
    function refresh_komentare(){
        $http.get('/getKomentare').then(function(res){
            $scope.komentar_list = res.data;
        }),
        function(res){
            alert(res.status);
        }
    }

    $scope.dodaj_korisnika = function() {
        $http.post('/dodajKorisnika', $scope.korisnik).then(function(data) {
            $scope.korisnik = null;
            $scope.korisnik_list.push(data);
            refresh_korisnik();
        });
    }

    $scope.dodaj_objavu = function() {
        $http.post('/dodajObjavu', $scope.objava).then(function(data) {
            $scope.objava = null;
            $scope.objava_list.push(data);
            refresh_objave();
        });
    }

    $scope.dodaj_sliku_hrane = function() {
        $http.post('/dodajSlikuHrane', $scope.slika).then(function(data) {
            $scope.slika = null;
            $scope.slika_list.push(data);
            refresh_slike();  
        });
    }

    $scope.dodaj_komentar = function() {
        $http.post('/dodajKomentar', +$scope.komentar._id,$scope.komentar).then(function(data) {
            $scope.komentar = null;
            $scope.komentar_list.push(data);
            refresh_komentare();    
        });
    }

    $scope.izbrisi_korisnika = function(_id){
        $http.delete('/izbrisiKorisnika/'+_id).then(function(data){
          refresh_korisnik();
        });
    }

    $scope.izbrisi_objavu = function(_id){
        $http.delete('/izbrisiObjavu/'+_id).then(function(data){
          refresh_objave();
        });
    }

    $scope.izbrisi_sliku_hrane = function(_id){
        $http.delete('/izbrisiSlikuHrane/'+_id).then(function(data){
          refresh_slike();
        });
    }

    $scope.izbrisi_komentar = function(_id){
        $http.delete('/izbrisiKomentar/'+_id).then(function(data){
          refresh_komentare();
        });
    }

    $scope.update_Korisnik = function(){
        $http.put('/updateKorisnik/'+$scope.korisnik).then(function(data){
          refresh_korisnik();
          $scope.korisnik = null;
        });
    }  

    $scope.update_objavu = function(){
        $http.put('/updateObjavu/'+$scope.objava).then(function(data){
          refresh_objave();
          $scope.objava = null;
        });
    } 
    
    $scope.update_sliku_hrane = function(){
        $http.put('/updateSlikuHrane/'+$scope.slika).then(function(data){
          refresh_slike();
          $scope.slika = null;
        });
    } 

    $scope.update_komentar = function(){
        $http.put('/updateKomentar'+$scope.komentar).then(function(data){
          refresh_komentare();
          $scope.komentar = null;
        });
    } 

}