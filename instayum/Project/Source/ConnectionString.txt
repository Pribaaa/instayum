
import java.sql.Connection;
import java.sql.DatabaseMetaData; 
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet; 
import java.sql.ResultSetMetaData;
import java.sql.SQLException; 
import java.sql.Statement;
import java.util.ArrayList;
	
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.digester.Digester;


public class V1Konekcija {

static String korisnik = "BP09";
static String sifra = "o7uwfSxI"; 
static String driver = "oracle.jdbc.driver.OracleDriver"; //org.gjt.mm.oracle.Driver
static String url = "jdbc:oracle:thin:@ora.db.lab.ri.etf.unsa.ba:1521:ETFLAB";



protected int dbConnectionsMinCount = 4; 
protected int dbConnectionsMaxCount = 10;
protected int dbConnectionMaxWait = -1; 
protected BasicDataSource dataSource; 

static Connection konekcija = null;
private Connection connection; 
static Statement iskaz = null; 
static ResultSet rezultat = null; 
static DatabaseMetaData metaPodaci = null; 



public void loadDriver() { 
	try { // driver je staticka varijabla, definisana naprijed koja daje naziv drajvera
		Class.forName(driver); 
		System.out.println("Load-driver uspjesan !");
	}
	catch (ClassNotFoundException e) { 
		e.printStackTrace(); } 
}
	
	
public void connect() {
	try { 
		konekcija = DriverManager.getConnection(url, korisnik, sifra); 
		System.out.println("Connect...");
		}	
	catch (SQLException e) { e.printStackTrace(); } 
} 

public void diskonekt() {
try {
konekcija.close(); 
System.out.println("Disconnect...");} 

catch (SQLException e) { e.printStackTrace(); }
}

public synchronized void open() throws Exception { // Using commons-dbcp's BasicDataSource 
	try { dataSource = new BasicDataSource(); // instanciranje klase BasicDataSource 
	dataSource.setDriverClassName(driver); // setovanje naziva driver-a 
	dataSource.setUrl(url); // setovanje URL-a 
	dataSource.setUsername(korisnik); // setovanje naziva korisnika 
	dataSource.setPassword(sifra); // setovanje sifre 
	// setovanje minimalnog, maksimalnog broja konekcija kao i vremena cekanja 
	// za uspostavu konekcije
	dataSource.setMaxIdle(dbConnectionsMinCount); 
	dataSource.setMaxActive(dbConnectionsMaxCount); 
	dataSource.setMaxWait(dbConnectionMaxWait); }
	catch (Exception e) { e.printStackTrace(); } }
	

public Connection getConnection() { connection = null; 
try { connection = dataSource.getConnection(); } 
catch (SQLException e) { System.out.println("Greska - konekcija"); }
return connection; }


public Statement getStatement(Connection konekcija) { try 
{ // za datu konekciju kreira i vraca objekat klase Statement
	iskaz = konekcija.createStatement(); } 
catch (SQLException e) { e.printStackTrace(); } return iskaz; }



public static void main(String[] args)
{
V1Konekcija db = new V1Konekcija();
try {
db.loadDriver(); //napuni klasu drajver 
db.connect(); //uspostavi konekciju - konekcija
db.diskonekt(); // raskini konekciju - konekcija


}
catch (Exception e) { e.printStackTrace(); } 
}
}










